package com.booking.recruitment.hotel.utils;

/**
 *
 */
public class HaversineDistanceCalculator {

    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }

    public static Double distance(Double lat1, Double lon1, Double lat2, Double lon2) {
        final int R = 6371; // Radious of the earth
        Double latDistance = toRad(lat2 - lat1);
        Double lonDistance = toRad(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }
}
