package com.booking.recruitment.hotel.controller;

import com.booking.recruitment.hotel.exception.BadRequestException;
import com.booking.recruitment.hotel.model.Hotel;
import com.booking.recruitment.hotel.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/search")
public class SearchController {

    private static final String DISTANCE = "distance";
    private SearchService searchService;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping("/{cityId}")
    @ResponseStatus(HttpStatus.OK)
    public List<Hotel> getClosestHotel(@PathVariable Long cityId, @RequestParam("sortBy") String sortBy){
        if(DISTANCE.equals(sortBy)){
            return searchService.getClosestHotels(cityId);
        }
        throw new BadRequestException("Sorting not available");
    }
}
