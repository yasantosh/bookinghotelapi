package com.booking.recruitment.hotel.service;

import com.booking.recruitment.hotel.model.City;
import com.booking.recruitment.hotel.model.Hotel;
import com.booking.recruitment.hotel.utils.HaversineDistanceCalculator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;


@Service
public class SearchService {

    private CityService cityService;
    private HotelService hotelService;

    public SearchService(CityService cityService, HotelService hotelService) {
        this.cityService = cityService;
        this.hotelService = hotelService;
    }

    public List<Hotel> getClosestHotels(Long cityId) {
        City originCity = cityService.getCityById(cityId);
        if (originCity != null) {
            double lat1 = originCity.getCityCentreLatitude();
            double lon1 = originCity.getCityCentreLongitude();
            List<Hotel> allHotelsInCity = hotelService.getHotelsByCity(cityId);
            return extractClosestThree(lat1, lon1, allHotelsInCity);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    private List<Hotel> extractClosestThree(double lat1, double lon1, List<Hotel> allHotelsInCity) {

        PriorityQueue<HotelInfo> pq = new PriorityQueue<>((o1, o2) -> o2.distance.compareTo(o1.distance));

        for (Hotel hotel : allHotelsInCity) {
            double lat2 = hotel.getLatitude();
            double lon2 = hotel.getLongitude();
            Double distance = HaversineDistanceCalculator.distance(lat1, lon1, lat2, lon2);
            pq.add(new HotelInfo(distance, hotel));
            if(pq.size() > 3){
                pq.poll();
            }
        }
        List<Hotel> hotels = new ArrayList<>();
        while(!pq.isEmpty()){
            hotels.add(pq.poll().hotel);
        }
        return hotels;
    }

    private class HotelInfo {
        Double distance;
        Hotel hotel;

        public HotelInfo(double distance, Hotel hotel) {
            this.distance = distance;
            this.hotel = hotel;
        }
    }
}
